-module(lib_find).
-export([files/2, files/3, files/4, files/6]).
-import(lists, [reverse/1]).

-include_lib("kernel/include/file.hrl").

ignored_dir_fun_gen(IgnoredDir) ->
  fun(File) ->
    case sets:is_set(IgnoredDir) of
      true ->
        IgnoredDirSet = IgnoredDir;
      false ->
        IgnoredDirSet = sets:from_list(IgnoredDir)
    end,
    sets:is_element(File, IgnoredDirSet)
  end.

files(Dir, Re) ->
  files(Dir, Re, true).

files(Dir, Re, Recursive) ->

  % translate "*.png" -> ".*\\.png"
  {_, Rel} = re:compile(xmerl_regexp:sh_to_awk(Re), [unicode]),
  reverse(files(Dir, Rel, Recursive, fun(_) -> false end, fun(File, Acc) -> [File | Acc] end, [])).

files(Dir, Re, IgnoredDir, [ignore]) ->
  {_, Rel} = re:compile(xmerl_regexp:sh_to_awk(Re), [unicode]),
  files(Dir, Rel, true, ignored_dir_fun_gen(IgnoredDir), fun(File, Acc) -> [File | Acc] end, []).

files(Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc) ->

  case file:list_dir(Dir) of
    {ok, Files} ->
      find_files(Files, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc);
    {error, _} ->
      io:format("error open directory ~s~n", [Dir]),
      find_files([], Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc)
  end.

find_files([File | T], Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc0) ->
  FullName = filename:join([Dir, File]),
  case file_type(FullName) of
    regular ->
      case re:run(FullName, Reg, [{capture, none}]) of
        match ->
          Acc = Fun(FullName, Acc0),
          find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc);
        nomatch ->
          find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc0)
      end;
    directory ->
      case IgnoredDirFun(File) of  % only check basename
        true ->
          find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc0);
        false ->
          case Recursive of
            true ->
              Acc1 = files(FullName, Reg, Recursive, IgnoredDirFun, Fun, Acc0),
              find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc1);
            false ->
              find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc0)
          end
      end;
    error ->
      % skipp this file and go on.
      find_files(T, Dir, Reg, Recursive, IgnoredDirFun, Fun, Acc0)
  end;

find_files([], _, _, _, _, _, A) -> A.

file_type(File) ->
  case file:read_file_info(File) of
    {ok, Facts} ->
      case Facts#file_info.type of
        regular ->
          regular;
        directory -> directory;
        _ -> error
      end;
    _ -> error
  end.
