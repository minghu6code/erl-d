%%%-------------------------------------------------------------------
%%% @author minghu6
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 27. Jun 2018 9:59 PM
%%%-------------------------------------------------------------------
-module(lib_find_v3).
-author("minghu6").

-import(lib_parallel, [pmap/2, pmap2/2, pmap/3]).
%% API
-export([files/2, files/4, files_and_dirs/5, group_files/2]).

%% quick lib_find_v3 than lib_find than lib_find_v2

files_and_dirs([FileName|T], MatchedFiles, SubDirs, Rel, Dir) ->
    FullName = filename:join([Dir, FileName]),

    case lib_find_v2:file_type(FullName) of
            regular ->
                case re:run(FileName, Rel, [{capture, none}]) of
                    match ->
                        files_and_dirs(T, [FullName|MatchedFiles], SubDirs, Rel, Dir);
                    nomatch ->
                        files_and_dirs(T, MatchedFiles, SubDirs, Rel, Dir)
                end;
            directory ->
                files_and_dirs(T, MatchedFiles, [FullName|SubDirs], Rel, Dir);
            error ->
                io:format("error get type of ~s~n", [FullName]),
                files_and_dirs(T, MatchedFiles, SubDirs, Rel, Dir)
    end;
files_and_dirs([], MatchedFiles, SubDirs, _, _) ->
    {MatchedFiles, SubDirs}.

group_files(Dir, Re) ->
    case file:list_dir(Dir) of
        {ok, Files} ->
            {_, Rel} = re:compile(xmerl_regexp:sh_to_awk(Re), [unicode]),
            files_and_dirs(Files, [], [], Rel, Dir);

        {error, _} ->
            io:format("error open directory ~s~n", [Dir]),
            {[], []}
    end.

files0(Dir, Re, F) ->
    {MatchedFiles, SubDirs} = group_files(Dir, Re),
    MatchedFiles ++ lists:append(pmap(F, SubDirs)).

files(Dir, Re) ->
    files0(Dir, Re, fun(EachSubDir) -> lib_find:files(EachSubDir, Re) end).

files(Dir, Re, IgnoredDir, [ignore]) ->
    files0(Dir, Re, fun(EachSubDir) -> lib_find:files(EachSubDir, Re, IgnoredDir, [ignore]) end);

files(Dir, Re, K, [limit]) ->
    {MatchedFiles, SubDirs} = group_files(Dir, Re),
    MatchedFiles ++ lists:append(pmap(fun(EachSubDir) -> lib_find:files(EachSubDir, Re) end, SubDirs, K)).
