-module(afile_test).
%compile:file(afile_server).
%compile:file(afile_client).
-import(afile_server, [start/1]).
-import(afile_client, [ls/1, get_file/2, put_file/3]).

main(_)->
    FileServer = afile_server:start("."),
    io:format("~p~n", [afile_client:ls(FileServer)]),
    io:format("~p~n", [afile_client:get_file(FileServer, "afile_server.erl")]),
    io:format("~p~n", [afile_client:put_file(FileServer, "file", "hello, test file")]).
