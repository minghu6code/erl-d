.SUFFIXES: .erl .beam

.erl.beam:
	erlc -W $<

ERL = erl -boot start_clean

MODS = afile_server afile_client hello afile_test clock dist_demo lib_find lib_find_v2 lib_print processes stimer \
    lib_find_v3 lib_parallel lib_find_v4

PWD := $(shell pwd)

all: compile
	${ERL} -pa PWD -noshell -s afile_test start -s init stop

compile: ${MODS:%=%.beam}

clean:
	rm -rf *.beam erl_crash.dump