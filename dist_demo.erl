-module(dist_demo).

-export([rpc/4, start/1]).

start(Node) -> spawn(Node, fun () -> loop() end).

rpc(Pid, M, F, A) ->
    Pid ! {rpc, self(), M, F, A},
    receive {Pid, Response} -> Response end.

loop() ->
    receive
      {rpc, Pid, M, F, A} ->
	  Pid ! {self(), catch apply(M, F, A)}, loop()
    %   after 5000 ->
	% 	io:format("5000 ms timeout, re loop~n"), loop()
    end.
