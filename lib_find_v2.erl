-module(lib_find_v2).
% -export([files/3, files/5]).
-compile(export_all).
-import(lists, [reverse/1]).

-include_lib("kernel/include/file.hrl").


files(Dir, Re) ->
  {GatherPid, _} = spawn_monitor(?MODULE, gather_loop, [self()]),
  spawn_monitor(?MODULE, pfiles, [GatherPid, Dir, Re]),

  receive
    {getResult, Files} -> Files
  end.

ignored_dir_fun_gen(IgnoredDir) ->
  fun(File) ->
    case sets:is_set(IgnoredDir) of
      true ->
        IgnoredDirSet = IgnoredDir;
      false ->
        IgnoredDirSet = sets:from_list(IgnoredDir)
    end,
    sets:is_element(File, IgnoredDirSet)
  end.

files(Dir, Re, IgnoredDir, [ignore]) ->

  {GatherPid, _} = spawn_monitor(?MODULE, gather_loop, [self()]),
  spawn_monitor(?MODULE, pfiles, [GatherPid, Dir, Re, true, ignored_dir_fun_gen(IgnoredDir), fun(File, Acc) -> [File | Acc] end]),

  receive
    {getResult, Files} -> Files
  end.


gather_loop(MainPid) ->
  SubTaskPidTableId = ets:new(subTaskPid, [set]),
  gather_loop(SubTaskPidTableId, MainPid, []).

gather_loop(SubTaskPidTableId, MainPid, Files0) ->
  receive
    {Client, register, Dir} ->
      ets:insert(SubTaskPidTableId, {Client, Dir}),
      gather_loop(SubTaskPidTableId, MainPid, Files0);
    {Client, done, Files1} ->
      ets:delete(SubTaskPidTableId, Client),
      case ets:info(SubTaskPidTableId, size) of
        0 ->
          %lib_print:format_list(Files0 ++ Files1),
          MainPid ! {getResult, Files0 ++ Files1};
        _ ->
          gather_loop(SubTaskPidTableId, MainPid, Files0 ++ Files1)
      end;
    {_, error, Why} ->
      MainPid ! {error, Why}
  end.

pfiles(GatherPid, Dir, Re) ->
  pfiles(GatherPid, Dir, Re, true, fun(_) -> false end, fun(File, Acc) -> [File | Acc] end).

pfiles(GatherPid, Dir, Re, Recursive, IsIgnoredDirFun, Fun) ->

  % translate "*.png" -> ".*\\.png"
  {_, Rel} = re:compile(xmerl_regexp:sh_to_awk(Re), [unicode]),
  pfiles(GatherPid, Dir, Rel, Recursive, IsIgnoredDirFun, Fun, []).

pfiles(GatherPid, Dir, Reg, Recursive, FilterFun, Fun, Acc) ->
  case file:list_dir(Dir) of
    {ok, Files} ->
      pfind_files(GatherPid, Files, Dir, Reg, Recursive, FilterFun, Fun, Acc);
    {error, _} ->
      io:format("error open directory ~s~n", [Dir]),
      pfind_files(GatherPid, [], Dir, Reg, Recursive, FilterFun, Fun, Acc)
  end.

pfind_files(GatherPid, [File | T], Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0) ->
  FullName = filename:join([Dir, File]),
  case file_type(FullName) of
    regular ->
      case re:run(FullName, Reg, [{capture, none}]) of
        match ->
          Acc = Fun(FullName, Acc0),
          pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc);
        nomatch ->
          pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0)
      end;
    directory ->
      case IsIgnoredDirFun(File) of  % only check basename
        true ->
          pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0);
        false ->
          case Recursive of
            true ->
              {Pid, _} = spawn_monitor(?MODULE, pfiles, [GatherPid, FullName, Reg, Recursive, IsIgnoredDirFun, Fun, []]),
              GatherPid ! {Pid, register, FullName},
              pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0);
            false ->
              pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0)
          end
      end;
    error ->
      % skipp this file and go on.
      pfind_files(GatherPid, T, Dir, Reg, Recursive, IsIgnoredDirFun, Fun, Acc0)
  end;

pfind_files(GatherPid, [], _, _, _, _, _, A) -> GatherPid ! {self(), done, A}.

file_type(File) ->
  case file:read_file_info(File) of
    {ok, Facts} ->
      case Facts#file_info.type of
        regular ->
          regular;
        directory -> directory;
        _ -> error
      end;
    _ -> error
  end.