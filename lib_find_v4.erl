-module(lib_find_v4).
-author("minghu6").

-import(lib_parallel, [mapreduce/4]).
%% API
-export([files/2]).

files(Dir, Re) ->
    {MatchedFiles, SubDirs} = lib_find_v3:group_files(Dir, Re),
    lists:append(MatchedFiles, 
    mapreduce(
        fun(ReducedId, EachSubDir) -> ReducedId ! {self(), lib_find:files(EachSubDir, Re)} end,
        fun(_, Value, Acc) -> lists:append(Value, Acc) end,
        [],
        SubDirs)).