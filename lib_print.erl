-module(lib_print).
-compile(export_all).


format_list(L) when is_list(L) ->
        io:format("["),
        fnl(L),
        io:format("]").

    fnl([H]) ->
        io:format("~p", [H]);
    fnl([H|T]) ->
        io:format("~p,", [H]),
        fnl(T);
    fnl([]) ->
        ok.

list(L, all) ->
  [io:format("~p~n", [X]) || X <- L].