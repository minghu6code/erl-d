-module(hello).

-export([max/2, perms/1, pythag/1, qsort/1, start/0,
	 start/1]).

start() -> io:format("Hello, World~n").

start(List) -> start(), qsort(List).

% python
% def qsort(l):
%     if not l:
%         return l
%     else:
%         privot, *tail = l
%         return qs([x for x in tail if x < privot]) + [privot] + qs([x for x in tail if x >= privot])

qsort([]) -> [];
qsort([Privot | T]) ->
    qsort([X || X <- T, X < Privot]) ++
      [Privot] ++ qsort([X || X <- T, X >= Privot]).

pythag(N) ->
    [{A, B, C}
     || A <- lists:seq(1, N), B <- lists:seq(1, N),
	C <- lists:seq(1, N), A + B + C =< N,
	A * A + B * B =:= C * C].

perms([]) -> [[]];
perms(L) -> [[H | T] || H <- L, T <- perms(L -- [H])].

max(X, Y) when X > Y -> X;
max(X, Y) -> Y.
